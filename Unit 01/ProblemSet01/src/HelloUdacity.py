#!/usr/bin/env python
#################################################################################
# FILENAME   : HelloUdacity.py                                                  #
# AUTHOR     : Brian Denton <brian.denton@gmail.com>                            #
# DATE       : 01/25/2013                                                       #
# PROJECT    : Udacity -- Web Development -- Problem Set 1                      #
# DESCRIPTION: First app on Google App Engine                                   #
#################################################################################

import webapp2

form = """
<form action="http://www.google.com/search">

<input name="q">
<input type="submit">

</form>
"""

class MainPage(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.write('Hello, Udacity!')

app = webapp2.WSGIApplication( [('/', MainPage)], debug=True)


## END OF FILE
