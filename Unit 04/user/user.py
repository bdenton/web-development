import os
import re
from string import letters

import webapp2
import jinja2
from hmac import new as hmac

from google.appengine.ext import db

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir),
                               autoescape = True)
SECRET = 'KawasakiEX300'
UsernameMap = {}
PasswordMap = {}

def hash_str(s):
    return hmac( SECRET, str(s) ).hexdigest()

def render_str(template, **params):
    t = jinja_env.get_template(template)
    return t.render(params)

class BaseHandler(webapp2.RequestHandler):

    def render(self, template, **kw):
        self.response.out.write(render_str(template, **kw))

    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
def valid_username(username):
    return username and USER_RE.match(username)

PASS_RE = re.compile(r"^.{3,20}$")
def valid_password(password):
    return password and PASS_RE.match(password)

EMAIL_RE  = re.compile(r'^[\S]+@[\S]+\.[\S]+$')
def valid_email(email):
    return not email or EMAIL_RE.match(email)

class Signup(BaseHandler):

    def get(self):
        self.response.headers['Content-Type'] = 'text/html'
        self.render("signup-form.html")

    def post(self):
        have_error = False
        username = self.request.get('username')
        password = self.request.get('password')
        verify = self.request.get('verify')
        email = self.request.get('email')

        params = dict(username = username,
                      email = email)

        userhash = hash_str( str(username) )

        if not valid_username(username):
            params['error_username'] = "That's not a valid username."
            have_error = True

        if UsernameMap.get( userhash ) != None:
            params['error_username'] = "Username not available."
            have_error = True

        if not valid_password(password):
            params['error_password'] = "That wasn't a valid password."
            have_error = True

        elif password != verify:
            params['error_verify'] = "Your passwords didn't match."
            have_error = True

        if not valid_email(email):
            params['error_email'] = "That's not a valid email."
            have_error = True

        if have_error:
            self.render('signup-form.html', **params)

        else:
            UsernameMap[ userhash ] = str( username )
            PasswordMap[ userhash ] = str( password )

            self.response.headers.add_header( 'Set-Cookie', 'user=%s; Path=/' % str(userhash) )
            self.redirect( '/unit04/welcome' )


class Login(BaseHandler):

    def get(self):
        self.response.headers['Content-Type'] = 'text/html'
        self.render("login-form.html")

    def post(self):
        have_error = False

        login_name = self.request.get('username')
        login_pass = self.request.get('password')

        userhash = hash_str( str(login_name) )

        params = {}

        if UsernameMap.get( userhash ) != str(login_name):
            params['error_username'] = "Username incorrect."
            have_error = True

        if PasswordMap.get( userhash ) != str(login_pass):
            params['error_password'] = "Password incorrect."
            have_error = True

        if have_error:
            self.render('login-form.html', **params )

        else:
            self.response.headers.add_header( 'Set-Cookie', 'user=%s; Path=/' % str(userhash) )
            self.redirect( '/unit04/welcome' )


class Logout(BaseHandler):

    def get(self):
        self.response.headers.add_header( 'Set-Cookie', 'user=%s; Path=/' % str("") )
        self.redirect( '/unit04/signup' )


class Welcome(BaseHandler):

    def get(self):
        
        userhash = self.request.cookies.get('user') 

        username = UsernameMap.get( userhash )

        if valid_username( username ):
             self.render('welcome.html', username = username )
        else:
             self.redirect('/unit04/signup')

class ClearMap(BaseHandler):

    def get(self):
        UsernameMap.clear()
        PasswordMap.clear()
        self.redirect('/unit04/signup')

class PrintMap(BaseHandler):

    def get(self):
        print "got here"
        print UsernameMap
        print PasswordMap

app = webapp2.WSGIApplication([('/unit04/login', Login),
                               ('/unit04/logout', Logout),
                               ('/unit04/signup', Signup),
                               ('/unit04/welcome', Welcome),
                               ('/unit04/clear', ClearMap),
                               ('/unit04/print', PrintMap)],
                               debug = True)
