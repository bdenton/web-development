import webapp2
import datetime

from google.appengine.api import memcache

KEY = "KEY"

class StringCache:
  
  def __init__( self, string ):
    memcache.set( KEY, ( string, datetime.datetime.now() ) )



class BaseHandler(webapp2.RequestHandler):

  def write(self, *a, **kw):
    self.response.out.write(*a, **kw)


class SetValue( BaseHandler ):

  def get(self):
    StringCache( 'This is a test.' )


class GetValue( BaseHandler ):

  def get(self):

   cached_results = memcache.get( KEY )
   
   if cached_results:
     string, cached_datetime = cached_results

     seconds_since_cached = (datetime.datetime.now() - cached_datetime).total_seconds()
      
     self.write( "%s; cached %f seconds ago." % ( string, seconds_since_cached ) )


class Flush( BaseHandler ):

  def get( self ):
    memcache.flush_all()


app = webapp2.WSGIApplication( [('/set', SetValue),
                                ('/get', GetValue),
                                ('/flush', Flush) ], 
                                debug = True )
