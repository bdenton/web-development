import os
import webapp2
import jinja2
from google.appengine.api import memcache
from google.appengine.ext import db
import json
from string import letters
from hmac import new as hmac
import re
import datetime

template_dir = os.path.join( os.path.dirname(__file__), 'templates' )
jinja_env = jinja2.Environment( loader = jinja2.FileSystemLoader(template_dir), 
                                autoescape=True)

#SECRET = 'KawasakiEX300'
# key = 'POSTS'
# UsernameMap = {}
# PasswordMap = {}

# def hash_str(s):
#     return hmac( SECRET, str(s) ).hexdigest()

def render_str(template, **params):
    t = jinja_env.get_template(template)
    return t.render(params)


def age_set(key,val):
    cached_datetime = datetime.datetime.now()
    memcache.set( key, (val, cached_datetime))

def age_get(key):
    r = memcache.get(key)
    if r:
        val, cached_datetime = r
        age = (datetime.utcnow() - cached_datetime).total_seconds()
        return val, age
    else:
        val, age = None, 0
    return val, age


def add_post( post ):
    post.put()
    get_posts( update = True )
    return( str( post.key().id() ) )


def get_posts( update = False ):
    q = greetings = Post.all().order('-created').fetch( limit = 10 )
    mc_key = 'BLOGS'

    posts, age = age_get('mc_key')
    if update or posts is None:
        posts = list(q)
        age_set(mc_key,posts)

    return posts, age





USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
def valid_username(username):
    return username and USER_RE.match(username)

PASS_RE = re.compile(r"^.{3,20}$")
def valid_password(password):
    return password and PASS_RE.match(password)

EMAIL_RE  = re.compile(r'^[\S]+@[\S]+\.[\S]+$')
def valid_email(email):
    return not email or EMAIL_RE.match(email)


class BaseHandler(webapp2.RequestHandler):

    def render(self, template, **kw):
        self.response.out.write(render_str(template, **kw))

    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

class Signup(BaseHandler):

    def get(self):
        self.response.headers['Content-Type'] = 'text/html'
        self.render("signup-form.html")

    def post(self):
        have_error = False
        username = self.request.get('username')
        password = self.request.get('password')
        verify = self.request.get('verify')
        email = self.request.get('email')

        params = dict(username = username,
                      email = email)

        userhash = hash_str( str(username) )

        if not valid_username(username):
            params['error_username'] = "That's not a valid username."
            have_error = True

        if UsernameMap.get( userhash ) != None:
            params['error_username'] = "Username not available."
            have_error = True

        if not valid_password(password):
            params['error_password'] = "That wasn't a valid password."
            have_error = True

        elif password != verify:
            params['error_verify'] = "Your passwords didn't match."
            have_error = True

        if not valid_email(email):
            params['error_email'] = "That's not a valid email."
            have_error = True

        if have_error:
            self.render('signup-form.html', **params)

        else:
            UsernameMap[ userhash ] = str( username )
            PasswordMap[ userhash ] = str( password )

            self.response.headers.add_header( 'Set-Cookie', 'user=%s; Path=/' % str(userhash) )
            self.redirect( '/unit06/blog/welcome' )


class Login(BaseHandler):

    def get(self):
        self.response.headers['Content-Type'] = 'text/html'
        self.render("login-form.html")

    def post(self):
        have_error = False

        login_name = self.request.get('username')
        login_pass = self.request.get('password')

        userhash = hash_str( str(login_name) )

        params = {}

        if UsernameMap.get( userhash ) != str(login_name):
            params['error_username'] = "Username incorrect."
            have_error = True

        if PasswordMap.get( userhash ) != str(login_pass):
            params['error_password'] = "Password incorrect."
            have_error = True

        if have_error:
            self.render('login-form.html', **params )

        else:
            self.response.headers.add_header( 'Set-Cookie', 'user=%s; Path=/' % str(userhash) )
            self.redirect( '/unit06/blog/welcome' )


class Logout(BaseHandler):

    def get(self):
        self.response.headers.add_header( 'Set-Cookie', 'user=%s; Path=/' % str("") )
        self.redirect( '/unit06/blog/signup' )


class Welcome(BaseHandler):

    def get(self):
        
        userhash = self.request.cookies.get('user') 

        username = UsernameMap.get( userhash )

        if valid_username( username ):
             self.render('welcome.html', username = username )
        else:
             self.redirect('/unit06/blog/signup')

# class ClearMap(BaseHandler):

#     def get(self):
#         UsernameMap.clear()
#         PasswordMap.clear()
#         self.redirect('/unit06/blog/signup')

# class PrintMap(BaseHandler):

#     def get(self):
#         print UsernameMap
#         print PasswordMap

class Post( db.Model ):
    subject = db.StringProperty( required = True )
    content = db.TextProperty( required = True )
    created = db.DateTimeProperty( auto_now_add = True )

    def as_dict(self):
        time_fmt = '%c'
        d = {'subject': self.subject,
             'content': self.content,
             'created': self.created.strftime(time_fmt)}
        return d

class Handler( webapp2.RequestHandler ):

    def write( self, *a, **kw ):
        self.response.out.write(*a, **kw)

    def render_str( self, template, **params ):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render( self, template, **kw ):
        self.write( self.render_str(template, **kw) )

    def render_json(self, d):
        json_txt = json.dumps(d)
        self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
        self.write(json_txt)

    def set_secure_cookie(self, name, val):
        cookie_val = make_secure_val(val)
        self.response.headers.add_header(
            'Set-Cookie',
            '%s=%s; Path=/' % (name, cookie_val))

    def read_secure_cookie(self, name):
        cookie_val = self.request.cookies.get(name)
        return cookie_val and check_secure_val(cookie_val)

    def initialize(self, *a, **kw):
        webapp2.RequestHandler.initialize(self, *a, **kw)
        uid = self.read_secure_cookie('user_id')
        self.user = uid and User.by_id(int(uid))
        if self.request.url.endswith('.json'):
            self.format = 'json'
        else:
            self.format = 'html'
        self.cached_datetime = -1

    # def get_posts( self, key, update = False ):

    #     posts = memcache.get(key)

    #     if posts is None or update:
    #         webapp2.logging.error("DB QUERY")
    #         posts = db.GqlQuery("SELECT * FROM Post ORDER BY created DESC LIMIT 10")
    #         posts = list(posts)
    #         cached_datetime = datetime.datetime.now()
    #         print "Got here", posts, cached_datetime
    #         memcache.set( key, (posts, cached_datetime) )
    #         update = False

    #     return posts


class NewPost( Handler ):

    def render2( self, subject="", content="", error="" ):
        self.render( "new_post.html", subject=subject, content=content, error=error )

    def get( self ):
        self.render2()

    def post(self):
        subject = self.request.get("subject")
        content = self.request.get("content")
            
        if subject and content:
            p = Post( subject = subject, content = content )
            p.put()
            get_posts( update = True )
            post_id = str( p.key().id() )
            self.redirect("/unit06/blog/%s" % post_id )
        
        else:
            error = "We need both a subject and content!"
            self.render2( subject = subject, content = content, error = error )


class PostHandler( Handler ):

    def get(self, post_id):
        post_key = 'POST_' + post_id
        
        post, age = age_get( post_key )
        
        if post:
            print "Got here", age
            key = db.Key.from_path('Post', int(post_id), parent=blog_key())
            post = db.get(key)
            age_set( post_key, post )
            age = 0

        if not post:
            self.error(404)
            return
        if self.format == 'html':
            self.render("permalink.html", post = post, age = age_str(age) )
        else:
            self.render_json(post.as_dict())





    # def get(self, post_id):

    #     post_key = 'POST_' + post_id
        
    #     post, age = age_get( post_key )
        
    #     if post:
    #         key = db.Key.from_path('Post', int(post_id), parent=blog_key())
    #         post = db.get(key)
    #         age_set( post_key, post )
    #         #age = 0

    #     key = db.Key.from_path('Post', int(post_id) )
    #     post = db.get(key)

    #     if not post:
    #         self.error( 404 )
    #         return

    #     if self.format == 'html':
    #         self.render("blog.html", posts = [post], age = age )
    #     else:
    #         self.render_json( post.as_dict())


class BlogHandler( Handler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        params['user'] = self.user
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))


class Blog( BlogHandler ):

    def get( self ):

        cached_posts = get_posts( update = True )

        if cached_posts[0]:

            #print "Got here"

            posts, cached_datetime = cached_posts

            if self.format == "html":
                print "Get to html"
                rendered_datetime = datetime.datetime.now()
                print "cached_datetime: ", cached_datetime
                print "rendered_datetime: ", rendered_datetime
                age = -99#int( round ( (rendered_datetime - cached_datetime).total_seconds() ) )
                self.render( "blog.html", posts = posts, age = age )
            else:
                return self.render_json([p.as_dict() for p in posts])


class Flush( BlogHandler ):
    def get( self ):
        memcache.flush_all()
        self.redirect( '/unit06/blog' )

app = webapp2.WSGIApplication( [('/unit06/blog/?(?:\.json)?', Blog ),
                                ('/unit06/blog/newpost', NewPost ),
                                ('/unit06/blog/([0-9]+)/?(?:\.json)?', PostHandler ),
                                ('/unit06/blog/login', Login),
                                ('/unit06/blog/logout', Logout),
                                ('/unit06/blog/signup', Signup),
                                ('/unit06/blog/welcome', Welcome),
                                ('/unit06/blog/flush', Flush)],
                                debug = True )

