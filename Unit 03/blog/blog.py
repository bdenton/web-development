import os
import webapp2
import jinja2
from google.appengine.ext import db

template_dir = os.path.join( os.path.dirname(__file__), 'templates' )
jinja_env = jinja2.Environment( loader = jinja2.FileSystemLoader(template_dir), 
                                autoescape=True)

class Post( db.Model ):
    subject = db.StringProperty( required = True )
    content = db.TextProperty( required = True )
    created = db.DateTimeProperty( auto_now_add = True ) 

class Handler( webapp2.RequestHandler ):
    def write( self, *a, **kw ):
        self.response.out.write(*a, **kw)

    def render_str( self, template, **params ):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render( self, template, **kw ):
        self.write( self.render_str(template, **kw) )


class NewPost( Handler ):

    def render2( self, subject="", content="", error="" ):
        self.render( "new_post.html", subject=subject, content=content, error=error )

    def get( self ):
        self.render2()

    def post(self):
        subject = self.request.get("subject")
        content = self.request.get("content")
            
        if subject and content:
            p = Post( subject = subject, content = content )
            p.put()
            post_id = str( p.key().id() )
            self.redirect("/unit03/blog/%s" % post_id )
        
        else:
            error = "We need both a subject and content!"
            self.render2( subject = subject, content = content, error = error )


class PostHandler( Handler ):

    def get( self, post_id ):
        
        p = Post.get_by_id( int(post_id) )
        if p:
            self.render( "blog.html", posts = [p]  )



class Blog( Handler ):
    
    def render2( self ):
        posts = db.GqlQuery( "SELECT * FROM Post ORDER BY created DESC LIMIT 10" )
        self.render( "blog.html", posts = posts )

    def get( self ):
        self.render2()



app = webapp2.WSGIApplication( [( '/unit03/blog', Blog ),
                                ( '/unit03/blog/newpost', NewPost ),
                                ( '/unit03/blog/([0-9]+)', PostHandler )], 
                                debug = True )

