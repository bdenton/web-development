#!/usr/bin/env python
#################################################################################
# FILENAME   : GoogleSearchForm.py                                              #
# AUTHOR     : Brian Denton <brian.denton@gmail.com>                            #
# DATE       : 01/27/2013                                                       #
# PROJECT    : Udacity -- Web Development --                                    #
# DESCRIPTION:                                                                  #
#################################################################################

import webapp2

form = """
<form method="get" action="/testform">
<!--
<input type="checkbox" name="q" value="one">
<input type="checkbox" name="r" value="two">
<input type="checkbox" name="s" value="three">

<label>One<input type="radio" name="q" value="one"></label>
<label>Two<input type="radio" name="q" value="two"></label>
<label>Three<input type="radio" name="q" value="three"></label>
-->

<select name="q">
  <option value="1">the number one</option>
  <option value="2">two</option>
  <option>three</option>
</select>
-->
<br>
<input type="submit">

</form>
"""

class MainPage(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/html'
        self.response.write( form )

class TestHandler( webapp2.RequestHandler ):
    def get( self ):
        q = self.request.get("q")
        #self.response.write( q )
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.out.write( self.request )

app = webapp2.WSGIApplication( [('/', MainPage),('/testform', TestHandler)],
                               debug=True)


## END OF FILE
