#!/usr/bin/env python
#################################################################################
# FILENAME   : ROT13.py                                                         #
# AUTHOR     : Brian Denton <brian.denton@gmail.com>                            #
# DATE       : 01/28/2013                                                       #
# PROJECT    : Udacity -- Web Development -- Problem Set 2                      #
# DESCRIPTION: ROT13 cipher                                                     #
#################################################################################

import webapp2

FORM1 = """
<html>
  <head>
    <title>Unit 2 Rot 13</title>
  </head>

  <body>
    <h2>Enter some text to ROT13:</h2>
    <form method="post">
    <textarea name="text" style="height: 100px; width: 400px;">"""

FORM2 = """</textarea>
    <br>

    <input type="submit">

  </body>
  </html>
</form>
"""

from cgi import escape
from string import maketrans, translate

def escape_html( s ):
    return escape( s, quote = True )

def rot13( s ):
    IN_TABLE  = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    OUT_TABLE = 'nopqrstuvwxyzabcdefghijklmNOPQRSTUVWXYZABCDEFGHIJKLM'

    trans_table = maketrans( IN_TABLE, OUT_TABLE )

    return str(s).translate( trans_table )
    

class MainPage(webapp2.RequestHandler):

    def write_form( self, content = "" ):
        form = FORM1 + content + FORM2
        self.response.out.write( form )

    def get(self):
        self.response.headers['Content-Type'] = 'text/html'
        self.write_form()

    def post(self):
        user_text = self.request.get( 'text' )
        self.write_form( escape_html( rot13( user_text ) ) )

app = webapp2.WSGIApplication( [('/', MainPage)], debug = True)


## END OF FILE
