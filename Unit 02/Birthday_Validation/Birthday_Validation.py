#!/usr/bin/env python
#################################################################################
# FILENAME   : Birthday_Validation.py                                           #
# AUTHOR     : Brian Denton <brian.denton@gmail.com>                            #
# DATE       : 01/28/2013                                                       #
# PROJECT    : Udacity -- Web Development --                                    #
# DESCRIPTION:                                                                  #
#################################################################################

import webapp2

form = """
<form method="post">
  What is your birthday?
  <br>

  <label>Month
  <input type="text" name="month" value="%(month)s">
  </label>

  <label> Day
  <input type="text" name="day" value="%(day)s">
  </label>

  <label> Year
  <input type="text" name="year" value="%(year)s">
  </label>

 <div style="color: red">%(error)s</div>

  <br>  
  <br>
  <input type="submit">
</form>
"""

from cgi import escape

from months import valid_month
from days import valid_day
from years import valid_year

def escape_html( s ):
    return escape( s, quote = True )

class MainPage(webapp2.RequestHandler):

    def write_form( self, error = "", month = "", day = "", year = "" ):
        self.response.out.write( form % {'error':error,
                                         'month':escape_html( month ),
                                         'day':escape_html( day ),
                                         'year':escape_html( year )} )

    def get(self):
        self.response.headers['Content-Type'] = 'text/html'
        #self.response.write( form )
        self.write_form()

    def post( self ):

        user_month = self.request.get( 'month' )
        user_day = self.request.get( 'day' )
        user_year = self.request.get( 'year' )

        month = valid_month( user_month )
        day = valid_day( user_day )
        year = valid_year( user_year )
        
        if not (month and day and year ):
            #self.response.out.write( form )
            self.write_form( "This doesn't look valid to me.",
                             user_month, user_day, user_year )
        else:
            self.redirect("/thanks")

class ThanksHandler(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/html'
        self.response.out.write( "Thanks! That's a totally valid day!" )

app = webapp2.WSGIApplication( [('/', MainPage),("/thanks",ThanksHandler)], debug = True )


## END OF FILE
